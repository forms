#
#	Forms makefile
#

DOXGEN = doxygen
DIRS = src test
INSTALL_ROOT = /usr/local

all:		lib demo doc

dist:		clean dep all

depend dep:
		@echo Making depend.
		@for dir in $(DIRS); do\
			(cd $$dir;$(MAKE) dep);\
		done

docs:
		(cd doc; rm -rf html latex; $(DOXGEN) forms.cfg;)

pdf:		docs
		(cd doc/latex; make refman.pdf)

demo:		
		(cd test; $(MAKE))

lib:		
		(cd src; $(MAKE))

install:	all
		rm -rf $(INSTALL_ROOT)/include/F
		cp -a F $(INSTALL_ROOT)/include
		cp -a libs/* $(INSTALL_ROOT)/lib
		ldconfig -n $(INSTALL_ROOT)/lib

clean:
		@echo Cleaning garbage.
		@for dir in $(DIRS); do\
			(cd $$dir;$(MAKE) clean);\
		done
		@rm -rf config.cache config.log
		@rm -rf doc/html doc/man doc/latex *out DEAD*

push:
		@git-push ssh://git@sfinx.od.ua/home/git/Harbour/forms

pull:
		@git-pull ssh://git@sfinx.od.ua/home/git/Harbour/forms

commit:
		@git-diff
		@git-commit -a
dcp:	commit	push

todo:		
		@setterm -foreground red
		@cat doc/TODO
		@echo
bugs:		
		@setterm -foreground red
		@cat doc/BUGS
		@echo

###
