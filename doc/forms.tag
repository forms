<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="page">
    <name>index</name>
    <title>����������� ������������</title>
    <filename>index</filename>
    <docanchor file="index">intro_sec</docanchor>
    <docanchor file="index">install_sec</docanchor>
  </compound>
  <compound kind="namespace">
    <name>F</name>
    <filename>namespaceF.html</filename>
    <class kind="class">F::F_App</class>
    <class kind="class">F::F_UI</class>
    <member kind="enumeration">
      <name>log_level_t</name>
      <anchor>a14</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>ui_class_t</name>
      <anchor>a15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const int</type>
      <name>FOREVER</name>
      <anchorfile>namespaceF.html</anchorfile>
      <anchor>a0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>F::F_App</name>
    <filename>classF_1_1F__App.html</filename>
    <member kind="function">
      <type></type>
      <name>F_App</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a0</anchor>
      <arglist>(int argc, char **argv, char *name=0, CommandOption *user_cmd_opts=defaultCommandOptionList, char *comment=0)</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>name</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>check</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a3</anchor>
      <arglist>(int ms)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>wait</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a4</anchor>
      <arglist>(int ms=FOREVER)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>run</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>idle</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a6</anchor>
      <arglist>(void(*cb)())</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log_level</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a7</anchor>
      <arglist>(log_level_t level)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>about</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>help</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>quit</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a10</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>init</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a11</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>log</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>a12</anchor>
      <arglist>(char *location, char *msg, log_level_t level=DEFAULT_LEVEL)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>flush</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>d0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private" static="yes">
      <type>static void</type>
      <name>signal_handler</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>h0</anchor>
      <arglist>(int)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>string</type>
      <name>name_</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>r0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CommandOptionParse *</type>
      <name>cmd_opts_</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>r1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>new_handler</type>
      <name>old_new_handler</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>r2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>in_idle_</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>r3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>void(*</type>
      <name>idle_</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>r4</anchor>
      <arglist>)()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>current_event_</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>r5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>log_level_t</type>
      <name>cur_log_level_</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>r6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static bool</type>
      <name>we_have_one_app_instance_</name>
      <anchorfile>classF_1_1F__App.html</anchorfile>
      <anchor>v0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>F::F_UI</name>
    <filename>classF_1_1F__UI.html</filename>
    <member kind="function">
      <type>void</type>
      <name>get_user_input</name>
      <anchorfile>classF_1_1F__UI.html</anchorfile>
      <anchor>a2</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
</tagfile>
