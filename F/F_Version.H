
 /*
  *   Copyright (C) 2007, Harbour, All rights reserved.
  */

#ifndef _F_VERSION_H_
#define _F_VERSION_H_

namespace F {

#define F_MAJOR_VERSION 0
#define F_MINOR_VERSION 1
#define F_PATCH_VERSION 0

// 24 bit value = 8 bit major | 8 bit minor | 8 bit patch

#define F_VERSION (((F_MAJOR_VERSION) << 16) + ((F_MINOR_VERSION) << 8) + \
                      (F_PATCH_VERSION))

}

#endif
