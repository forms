
 /*
  *   Copyright (C) 2007, Harbour, All rights reserved.
  */

#ifndef _F_TEXT_OUTPUT_H_
#define _F_TEXT_OUTPUT_H_

#include <F_Text_Display.H>
#include <F_Window.H>
#include <F_Scrollbar.H>
#include <cstdarg>

namespace F {

class F_Text_Output : public F_Widget {

  F_Wrap_t wrap_;
  F_Scrollbar *vs, *hs;
  // ���������� �����
  unsigned int size_;
  unsigned int topline_;
  std::vector <std::string> lines;
  char *line_buf;
  unsigned int line_buf_size;
  unsigned int max_line_size_;
  unsigned int start_col_; // starting column

  void draw();
  bool handle(F_Event_t &ev);
  void add_line(const char *str);
  static void vs_callback(F_Scrollbar *s, void *) {
    F_Text_Output *to = (F_Text_Output *)s->parent_wdg();
    if (s->value() > (to->lines.size() - (to->h() - 1)))
      to->topline(to->lines.size() - (to->h() - 1));
    else
      to->topline(s->value());
  }
  static void hs_callback(F_Scrollbar *s, void *) {
    F_Text_Output *to = (F_Text_Output *)s->parent_wdg();
    if (s->value() > (to->max_line_size() - (to->w() - 1)))
      to->startcol(to->max_line_size() - (to->w() - 1));
    else
      to->startcol(s->value());
  }

  void startcol(int sc) {
    if (sc < 0)
      sc = 0;
    start_col_ = sc;
    draw();
   }
 public:
  F_Text_Output(F_Box_Type_t btype, coord_t x, coord_t y, dim_t w, dim_t h,
    const char *label = 0) : F_Widget(x, y, w, h, label) {
      wrap_ = F_NO_WRAP;
      size_ = 0;
      topline_ = 0;
      max_line_size_ = 0;
      start_col_ = 0;
      line_buf_size = 10240U;
      line_buf = new char[line_buf_size];
      // hor slider is created with max width
      hs = new F_Scrollbar(F_NO_BOX, F_HORIZONTAL, x, y + h - 2, w, 1);
      vs = new F_Scrollbar(F_NO_BOX, F_VERTICAL, w + x - 1, y + 1, 1, h - 2);
      vs->callback((F_Callback *)vs_callback);
      hs->callback((F_Callback *)hs_callback);
      add_child(vs);
      add_child(hs);
 }
  ~F_Text_Output() { delete [] line_buf; }
  unsigned int max_line_size() { return max_line_size_; }
  unsigned int start_col() { return start_col_; }
  void topline(int tl) {
    if (tl < 0)
      tl = 0;
    if (tl > int(lines.size()))
      topline_ = lines.size();
    else
      topline_ = tl;
    draw();
  }
  void wrap(F_Wrap_t w) { wrap_ = w; draw(); }
  void clear() {
    lines.clear();
    topline_ = 0;
    start_col_ = 0; // ?
    max_line_size_ = 0;
    vs->value(0);
    hs->value(0);
    draw();
 }
  void add(std::string &s) { add_line(s.c_str()); }
  void add(const char *s) { add_line(s); }
  void addf(const char *fmt, ...) {
   ::va_list args;
   ::va_start(args, fmt);
   ::va_end(args);
   ::vsnprintf(line_buf, line_buf_size, fmt, args);
   add_line(line_buf);
  }
 };
};

#endif
