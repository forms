
 /*
  *   Copyright (C) 2007, Harbour, All rights reserved.
  */

#ifndef _F_FLTK_UI_H_
#define _F_FLTK_UI_H_

#include <F_UI.H>

namespace F {

/*! The simple x11 ui implemetation
 */

 class F_FLTK_UI : public F_UI {

   void run(void);
   bool probe();
   void initial(void);

  public:

   F_FLTK_UI(unsigned char ui_priority = 0) :
     F_UI(X11_UI, ui_priority) { }
   virtual ~F_FLTK_UI() { }
   void shutdown() { cleaned_up_ = true; }
   
 };
} // namespace F

#endif
