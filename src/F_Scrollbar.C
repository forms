
 /*
  *   Copyright (C) 2007, Harbour, All rights reserved.
  */

#include <F_Scrollbar.H>
#include <F_Window.H>

using namespace F;

void F_Scrollbar::draw()
{
 if (!bounds_)
   return;
 bgch(f_text_display->recode('�'));
 F_Widget::draw();
 F_Point p(0, 0);
 F_Text_Symbol smb;
 smb.color(parent()->fg(), parent()->bg());
 // draw arrows
 if (type_ == F_VERTICAL) {
   smb.ch_ = 0x18;
   draw_buf.set(p, smb);
   p.move(0, h() - 1);
   smb.ch_ = 0x19;
   draw_buf.set(p, smb);
 } else {
   smb.ch_ = 0x1B;
   draw_buf.set(p, smb);
   p.move(w() - 1, 0);
   smb.ch_ = 0x1A;
   draw_buf.set(p, smb);
 }
 // draw bar
 smb.ch_ = f_text_display->recode('�');
 unsigned int position;
 if (type_ == F_VERTICAL)
   position = (int)((((float)value_)/((float)bounds_)) * ((h() - 1) - size_));
 else
   position = (int)((((float)value_)/((float)bounds_)) * ((w() - 1) - size_));
 if (!position)
   position = 1;
 if (type_ == F_VERTICAL) {
   if (int(position + size_) > (h() - 1))
     position = (h() - 1) - size_;
 } else {
   if (int(position + size_) > (w() - 1))
     position = (w() - 1) - size_;
 }
 if (!position)
   position = 1;
 for (unsigned int i = 0; i < size_; i++) {
   if (type_ == F_VERTICAL)
     p.move(0, position + i);
   else
     p.move(position + i, 0);
   draw_buf.set(p, smb);
 }
}

static coord_t cur_mouse_x, cur_mouse_y;

bool F_Scrollbar::handle(F_Event_t &ev)
{
 switch(ev.type) {
   case F_ENTER:
   case F_FOCUS:
//     debug("F_ENTER: sbrid - %d", id());
     draw();
     return true;
   case F_LEAVE:
   case F_UNFOCUS:
//     debug("F_LEAVE: sbrid - %d", id());
     draw();
     return true;
   case F_KEY_PRESS:
     switch (ev.kbd.key) {
       case F_Page_Down:
         if (type_ == F_VERTICAL)
           value(value() + (h() - 1));
         else
           value(value() + (w() - 1));
         break;
       case F_Page_Up:
         if (type_ == F_VERTICAL)
           value(value() - (h() - 1));
         else
           value(value() - (w() - 1));
         break;
       case F_Up:
         if (type_ == F_VERTICAL)
           value(value() - 1);
         break;
       case F_Down:
         if (type_ == F_VERTICAL)
           value(value() + 1);
         break;
       case F_Left:
         if (type_ == F_HORIZONTAL)
           value(value() - 1);
         break;
       case F_Right:
         if (type_ == F_HORIZONTAL)
           value(value() + 1);
         break;
       case F_Home:
         if (ev.kbd.modifiers & F_CTRL) {
           value(0);
           break;
      }
       case F_End:
         if (ev.kbd.modifiers & F_CTRL) {
           value(bounds_);
           break;
      }
       default:
         return false;
     }
       draw();
       do_callback();
       return true;
   case F_KEY_RELEASE:
     break;
   case F_POINTER_DRAG:
//     debug("F_POINTER_DRAG: sbrid - %d", id());
     if (type_ == F_HORIZONTAL) { // sense x movement
       int x = ev.mouse.x - cur_mouse_x;
       if (x > 0)
         value(value() + 1);
       else if (x < 0)
         value(value() - 1);
       else
         break;
     } else {
       int y = ev.mouse.y - cur_mouse_y;
       if (y > 0)
         value(value() + 1);
       else if (y < 0)
         value(value() - 1);
       else
         break;
     }
     draw();
     do_callback();
   case F_POINTER_PRESS:
     // check if bar or space pressed
     // ....
     cur_mouse_x = ev.mouse.x;
     cur_mouse_y = ev.mouse.y;
     return true;
   default:
//     debug("sbrid - %d, evtype - %d", id(), ev.type);
     break;
 }
 return false;
}
