
 /*
  *   Copyright (C) 2007, Harbour, All rights reserved.
  */

#include <F_Gpm.H>
#include <F_Log.H>
#include <gpm.h>
#include <dlfcn.h>
#include <errno.h>

using namespace F;

static int (*Gpm_Open_)(Gpm_Connect *, int);
static int (*Gpm_Close_)(void);
static int (*Gpm_GetEvent_)(Gpm_Event *);
static int *gpm_zerobased_;
static int *gpm_visiblepointer_;
static Gpm_Connect gc;
static int gpm_fd_;

//#define DYNAMIC_GPM

F_Gpm::F_Gpm()
{
 gpm_fd_ = -1;
 int modes;
#ifdef DYNAMIC_GPM
 gpm_so = dlopen("libgpm.so", RTLD_NOW);
 if (!gpm_so)
   debug("Error opening libgpm.so: %s", dlerror());
 Gpm_GetEvent_ = (int (*)(Gpm_Event*)) dlsym(gpm_so, "Gpm_GetEvent");
 if (dlerror())
   goto no_gpm;
 Gpm_Close_ = (int (*)()) dlsym(gpm_so, "Gpm_Close");
 if (dlerror())
   goto no_gpm;
 Gpm_Open_ = (int (*)(Gpm_Connect*, int)) dlsym(gpm_so, "Gpm_Open");
 if (dlerror())
  goto no_gpm;
 gpm_zerobased_ = (int *) dlsym(gpm_so, "gpm_zerobased");
 if (dlerror())
  goto no_gpm;
 gpm_visiblepointer_ = (int *) dlsym(gpm_so, "gpm_visiblepointer");
 if (dlerror())
  goto no_gpm;
#else
 Gpm_GetEvent_ = Gpm_GetEvent;
 Gpm_Close_ = Gpm_Close;
 Gpm_Open_ = Gpm_Open;
 gpm_zerobased_ = &gpm_zerobased;
 gpm_visiblepointer_ = &gpm_visiblepointer;
#endif
 gc.minMod = 0;
 gc.maxMod = ~0;
 gc.defaultMask = 0;
 gc.eventMask = ~0;
 *gpm_zerobased_ = *gpm_visiblepointer_ = 1;
 gpm_fd_ = Gpm_Open_(&gc, 0);
 if (gpm_fd_ < 0)
   goto no_gpm;
 // ��������� gpm � non-blocking mode
 modes = fcntl(gpm_fd_, F_GETFL);
 if (modes < 0)
   goto no_gpm;
 if (fcntl(gpm_fd_, F_SETFL, modes | O_NONBLOCK) < 0)
   goto no_gpm;
 good_ = true;
 enabled_ = true; // enable mouse by default
 log("gpm", CHAT_LEVEL, "Found and enabled.");
 return;
no_gpm:
 good_ = false;
}

F_Gpm::~F_Gpm()
{
 if (gpm_fd_ > 0)
  bug_on(Gpm_Close_()); // only one connection is planned
 if (gpm_so)
   dlclose(gpm_so);
 gpm_so = 0;
}

static void map_event(Gpm_Event &ge, F_Event_t &event)
{
 event.dev = F::F_POINTER;
 event.type = F::F_EVENT_NONE;
 // mouse can be clicked, moved or dragged
 if (ge.type & GPM_MOVE)
     event.type = F::F_POINTER_MOVE;
 else if (ge.type & (GPM_MFLAG | GPM_DRAG))
     event.type = F::F_POINTER_DRAG;
 else if (ge.type & GPM_DOWN)
     event.type = F::F_POINTER_PRESS;
 // GPM_UP ����������� (GPM_MFLAG | GPM_DRAG) - ������� ��� F_DROP
 if (ge.type & GPM_UP)
     event.type = F::F_POINTER_RELEASE;
 if (ge.type & GPM_SINGLE)
   event.mouse.click = F::F_POINTER_SINGLE_CLICK;
 if (ge.type & GPM_DOUBLE)
   event.mouse.click = F::F_POINTER_DOUBLE_CLICK;
 if (ge.type & GPM_TRIPLE)
   event.mouse.click = F::F_POINTER_TRIPPLE_CLICK;
 // set new coordinates
 event.mouse.x = ge.x;
 event.mouse.y = ge.y;
 event.mouse.buttons = ((ge.buttons & GPM_B_LEFT) ? F_BUTTON1 : 0);
 event.mouse.buttons |= ((ge.buttons & GPM_B_MIDDLE) ?
   F_BUTTON2 : 0);
 event.mouse.buttons |= ((ge.buttons & GPM_B_RIGHT) ?
   F_BUTTON3 : 0);
}

// TODO: support gpm server restarts

bool F_Gpm::check_for_events(F_Event_t *event)
{
 if (!good_) // was unrecoverable error, may be need reconnecting to gpm
   return false;
 Gpm_Event ge;
 int res;
 switch ((res = Gpm_GetEvent_(&ge))) {
   case 0: // connection is closed
     good_ = false;
     break;
   case 1: // got event
     map_event(ge, *event);
     return true;
     break;
   default:
     if (errno == EAGAIN) // signal or non-blocking read incomplete
       return false;
     debug("error (%d) in Gpm_GetEvent [%d:%s], disabling mouse ...", res, errno, strerror(errno));
     good_ = false;
     break;
 }
 return good_;
}

void F_Gpm::redraw(bool v)
{ 
 if (v != visible_) { // change pointer state
 }
// if (ioctl(0, TIOCLINUX, &con_mou.tioc))
//   log(WARNING_LEVEL, "ioctl: %s", strerror(errno));
}
